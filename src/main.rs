//Copyright SoftwareByNumbers Ltd 2021

//! Test client for sa-md service
//!
//! Uses the keycloak security provider for access permissions.
//!


use futures::{stream, StreamExt};
use reqwest::Client;
use reqwest::header::AUTHORIZATION;
use tokio;
use std::iter::Iterator;
use std::sync::{Arc, Mutex};


const CONCURRENT_REQUESTS: usize = 100;

//How many requests to make
const NUM_URLS: usize = 100;
static TEST_URL: &str = "http://sa.softwarebynumbers.com:8000/api/model/23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb";

//Security token for the Auth header
static TOKEN: &str = "DUMMY TOKEN";


#[derive(Debug)]
struct Bad {
    message: Option<String>,
    code:    Option<String>
}

#[derive(Debug)]
struct ServiceResult {
    good: i64,
    failed: Vec<Bad>,
}


#[tokio::main]
async fn main() {

    let res: ServiceResult = ServiceResult{good: 0i64,failed: Vec::<Bad>::new()};

    //protected results
    let pres = Arc::new(Mutex::new(res));

    let client = Client::new();

    let urls = vec![TEST_URL; NUM_URLS];

    let bodies = stream::iter(urls)
        .map(|url| {
            let client = &client;
            async move {
                let resp = client.get(url).header(AUTHORIZATION, TOKEN).send().await;
                match resp {
                    Ok(r)  => {match r.error_for_status() {
                                  Ok(res)  => res.bytes().await,
                                  Err(err) => {
                                    std::result::Result::Err(err)
                                  },
                                }
                              },
                    Err(e) => std::result::Result::Err(e),//bytes:Bytes::from()
                }
            }
        })
        .buffer_unordered(CONCURRENT_REQUESTS);

    bodies
        .for_each(|b| async {
            match b {
                Ok(bytes) => {let msg = std::str::from_utf8(&bytes);
                              if msg.is_ok() {
                                    let mut res = pres.lock().unwrap();
                                    res.good += 1;
                              } else {

                                    let bad = Bad{message: Some(String::from("Message")), code: Some(String::from("0"))};
                                    let mut res = pres.lock().unwrap();
                                    res.failed.push(bad);

                              };},

                Err(e) => {let mut res = pres.lock().unwrap();
                           if e.is_status() {
                               let status = e.status();
                               match status {
                                   Some(s) => {let bad = Bad{message: Some(String::from(s.canonical_reason().unwrap())),
                                                             code: Some(String::from(s.as_str()))};
                                               res.failed.push(bad);},
                                   None    => {let bad = Bad{message: Some(String::from(format!("Error, was a status code but status was None {:?}", status))),
                                                             code:    Some(String::from("_"))};
                                               res.failed.push(bad);},
                               };
                           } else {
                               let bad = Bad{message: Some(String::from(format!("Error, not a status code {}", e))),
                                             code: Some(String::from("_"))};
                               res.failed.push(bad);
                           };},
         };


        })
        .await;
    let res = pres.lock().unwrap();
    let good_calls = res.good;
    let errors = &res.failed;

    println!("       RESULTS");
    println!("========================");
    println!("Good calls : {}", good_calls);
    println!("Error calls: {}", errors.len());
    println!("     ERROR DETAILS");
    println!("========================");

    let a = errors.iter().map(|e| -> String {
                                     let mut s = String::from(
                                     &match &e.message {
                                       Some(m) => format!("message: {}, ", m),
                                       None    => format!("message: none"),
                                     });
                                     s.push_str(
                                     &match &e.code {
                                         Some(c) => format!("code: {}, ", c),
                                         None    => format!("code: none"),
                                     });
                                     s
                                 });
    let s = a.collect::<Vec<String>>().join("\n");
    println!("{}", s);
}
