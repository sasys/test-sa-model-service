# Simple Test Harness For The sa-model-service.


## Run

```
cargo run
```

## Sample Output:
```
RESULTS
========================
Good calls : 0
Error calls: 100
ERROR DETAILS
========================
message: Forbidden, code: 403,
message: Forbidden, code: 403,
message: Forbidden, code: 403,
```

## Configure
Configure the number of urls, the test url and the user token in the source code.
